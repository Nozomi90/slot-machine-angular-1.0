import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  url_1: string ="/img/1.png";
  title = 'slotAngular';

  ngOnInit(){console.log(this.url_1)}


  private r1: number; private r1_old: number;
  private r2: number; private r2_old: number;
  private r3: number; private r3_old: number;
  private r4: number; private r4_old: number;
  private r5: number; private r5_old: number;

  changeR() {

    this.r1_old = this.r1;
    this.r2_old = this.r2;
    this.r3_old = this.r3;
    this.r4_old = this.r4;
    this.r5_old = this.r5;

    do {
      this.r1 = this.getRandomR();

      this.r2 = this.getRandomR();

      this.r3 = this.getRandomR();

      this.r4 = this.getRandomR();

      this.r5 = this.getRandomR();
    } while (this.r1 == this.r1_old || this.r2 == this.r2_old || this.r3 == this.r3_old || this.r4 == this.r4_old || this.r5 == this.r5_old);

    console.log('actual ' + this.r1, this.r2, this.r3, this.r4, this.r5);

  }


  getRandomR(): number {
    return Math.floor(Math.random() * 15 + 0);

  }


}